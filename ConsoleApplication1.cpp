﻿#include <iostream>
#include <string>

using namespace std;

int main()
{
	string str{ "First string in C++!" };

	cout << "String - " << str << endl;
	cout << "First elem - " << str.front() << endl;
	cout << "Last elem - " << str.back() << endl;
}